package com.cgi.jira.service;



import com.cgi.jira.persistence.entities.TicketEntity;
import com.cgi.jira.persistence.mappers.TicketMapper;

import java.util.List;

public interface TicketService {

    PersonById getPersonById(Long id);
    List<Person> getAllPersons();
}
    public List<TicketEntity> getAllTickets(){
        return jdbcTemplate.query("SELECT * FROM ticket", new TicketMapper());
    }

    public TicketEntity getTicketById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE id_ticket=?",
                new Object[]{id}, new TicketMapper());
    }

    public TicketEntity getTicketByName(String name) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE name=?",
                new Object[]{name}, new TicketMapper());
    }

    public void createNewTicket(){} //dodelat