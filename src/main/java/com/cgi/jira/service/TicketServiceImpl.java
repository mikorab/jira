package com.cgi.jira.service;

import org.cgi.persistence.entities.Person;
import org.cgi.persistence.repository.PersonRepository;
import org.cgi.ws.PersonById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {

    private PersonRepository personRepository;

    @Autowired
    public TicketServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public PersonById getPersonById(Long id) {
        Person person = personRepository.getPersonById(id);
        return mapPersonToPersonById(person);
    }

    private PersonById mapPersonToPersonById(Person person) {
        PersonById personById = new PersonById();
        personById.setId(person.getIdPerson());
        personById.setFirstName(person.getFirstName());
        personById.setSurname(person.getSurname());
        personById.setEmail(person.getEmail());
        personById.setBirthday(person.getBirthday());
        return personById;
    }

    @Override
    public List<Person> getAllPersons() {
        return personRepository.getAllPersons();
    }
}
