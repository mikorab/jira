package com.cgi.jira.persistence.mappers;

import com.cgi.jira.persistence.entities.TicketEntity;
import org.springframework.jdbc.core.RowMapper;

import javax.persistence.Column;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class TicketMapper implements RowMapper<TicketEntity> {
    @Override
    public TicketEntity mapRow(ResultSet resultSet, int i) throws SQLException {
        TicketEntity ticketEntity = new TicketEntity();
        ticketEntity.setName(resultSet.getString("name"));
        ticketEntity.setCreatorId(resultSet.getLong("id_person_creator"));
        ticketEntity.setPersonAssignedId(resultSet.getLong("id_person_assigned"));
        ticketEntity.setCreationDatetime(resultSet.getTimestamp("creation_datetime").toLocalDateTime());
        ticketEntity.setTicketCloseDatetime(resultSet.getTimestamp ("ticket_close_datetime").toLocalDateTime());
        return ticketEntity;
    }
}
