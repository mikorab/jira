package com.cgi.jira.persistence.soap.endpoints;

import org.cgi.service.PersonService;
import org.cgi.ws.GetPersonByIdRequest;
import org.cgi.ws.GetPersonByIdResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * <ul>
 * <li> @Endpoint -- Annotation to indicate that this is a Web Service Endpoint </li>
 * <li> @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPersonByIdRequest") -- Defines the details of the request that this method would handle. We will handle GetPersonByIdRequest with the given namespace </li>
 * <li> @ResponsePayload -- This method will return a response which would need to be converted to a response xml.</li>
 * <li> public getPersonById processCourseDetailsRequest(@RequestPayload GetStudentDetailsRequest request)-- Method would handle the request. @RequestPayload indicates that this is got from the request.</li>
 * </ul>
 */
@Endpoint
public class TicketEndpoint {

    private static final String NAMESPACE_URI = "http://cgi.org/ws";

    private PersonService personService;

    @Autowired
    public TicketEndpoint(PersonService personService) {
        this.personService = personService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetPersonByIdRequest")
    @ResponsePayload
    public GetPersonByIdResponse getPersonById(@RequestPayload GetPersonByIdRequest request) {
        GetPersonByIdResponse getPersonByIdResponse = new GetPersonByIdResponse();
        getPersonByIdResponse.setPersonById(personService.getPersonById(request.getId()));
        return getPersonByIdResponse;
    }

}
