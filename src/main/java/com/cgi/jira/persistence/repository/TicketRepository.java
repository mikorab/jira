package com.cgi.jira.persistence.repository;


import com.cgi.jira.persistence.entities.TicketEntity;
import com.cgi.jira.persistence.mappers.TicketMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class TicketRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<TicketEntity> getAllTickets(){
        return jdbcTemplate.query("SELECT * FROM ticket", new TicketMapper());
    }

    public TicketEntity getTicketById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE id_ticket=?",
                new Object[]{id}, new TicketMapper());
    }

    public TicketEntity getTicketByName(String name) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE name=?",
                new Object[]{name}, new TicketMapper());
    }

    public void createNewTicket(){} //dodelat

}
