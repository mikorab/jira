package com.cgi.jira.persistence.entities;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table (name = "ticket")
public class TicketEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id_ticket")
    private Long idTicket;

    private String name;

    @Column (name = "id_person_creator")
    private Long creatorId;

    @Column (name = "id_person_assigned")
    private Long personAssignedId;

    @Column (name = "creation_datetime")
    private LocalDateTime creationDatetime;

    @Column (name = "ticket_close_datetime")
    private LocalDateTime ticketCloseDatetime;

    public TicketEntity() {  }

    public Long getIdTicket() { return idTicket;}
    public void setIdTicket(Long idTicket) {this.idTicket = idTicket;}

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    public Long getCreatorId() { return creatorId;   }
    public void setCreatorId(Long creatorId) {     this.creatorId = creatorId;}

    public Long getPersonAssignedId() {   return personAssignedId;  }
    public void setPersonAssignedId(Long personAssignedId) {  this.personAssignedId = personAssignedId;  }

    public LocalDateTime getCreationDatetime() { return creationDatetime; }
    public void setCreationDatetime(LocalDateTime creationDatetime) {this.creationDatetime = creationDatetime; }

    public LocalDateTime getTicketCloseDatetime() {
        return ticketCloseDatetime;
    }

    public void setTicketCloseDatetime(LocalDateTime ticketCloseDatetime) {
        this.ticketCloseDatetime = ticketCloseDatetime;
    }

    @Override
    public String toString() {
        return "TicketEntity{" +
                "idTicket=" + idTicket +
                ", name='" + name + '\'' +
                ", creatorId=" + creatorId +
                ", personAssignedId=" + personAssignedId +
                ", creationDatetime=" + creationDatetime +
                ", ticketCloseDatetime=" + ticketCloseDatetime +
                '}';
    }
}
